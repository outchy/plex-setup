#!/bin/bash

# Plex Media Server Setup for Ubuntu
# Tested on 10.10, 11.10 and 12.10
# Written by Nik Outchcunis 
# outchy.org | outchy@gmail.com | @outchy

##############################
# Check to see if user is root
##############################

user=$(whoami)

if [[ $user = root ]]
	then
		echo; printf "Please rerun this script without using 'sudo'"; echo; echo
		exit 1
fi

###############
# Pre-checklist
###############

echo
printf "***********************"; echo
printf "Plex Media Server Setup"; echo
printf "***********************"; echo; echo

###########################
# Install Plex Media Server
###########################

if [ -d /usr/lib/plexmediaserver ]
	then
		printf "Plex Media Server is already installed, exiting..."; echo; echo
		exit 0
	else
		printf "ADDING PLEX REPOSITORY"
		
		sudo add-apt-repository "deb http://www.plexapp.com/repo lucid main"
		
		if [ $? -eq 0 ]
			then
				printf "... done."; echo; echo
				sudo sed -i '/deb-src http:\/\/www.plexapp.com\/repo lucid main/s/^/#/' /etc/apt/sources.list
			else
				printf "Repo add failed, exiting..."; echo; echo
				exit 1
		fi
		
		printf "DOWNLOADING PUBLIC RELEASE KEY FOR PLEX MEDIA SERVER"; echo; echo
		
		wget http://plexapp.com/repo/pool/main/p/plex-archive-keyring/plex-archive-keyring_2.0.0_all.deb -P /tmp
		
		if [ $? -ne 0 ]
			then
				echo; printf "wget failed, exiting."; echo; echo
				exit 1
			else
				printf "... done."; echo; echo
		fi
		
		printf "INSTALLING PUBLIC RELEASE KEY FOR PLEX MEDIA SERVER"; echo; echo
		
		sudo dpkg -i /tmp/plex-archive-keyring_2.0.0_all.deb
		
		if [ $? -eq 0 ]
			then
				echo; printf "... done."; echo; echo
			else
				echo; printf "Public release key installation failed, exiting..."; echo; echo
				exit 1
		fi
		
		printf "INSTALLING PLEX MEDIA SERVER"; echo; echo
		
		sudo apt-get update && sudo apt-get install plexmediaserver -y
		
		if [ $? -eq 0 ]
			then
				echo; printf "... done."; echo; echo
			else
				echo; printf "Plex Media Server installation failed, exiting..."; echo; echo
				exit 1
		fi

fi

printf "All set, Plex should now be running! Enjoy."; echo; echo

exit 0
